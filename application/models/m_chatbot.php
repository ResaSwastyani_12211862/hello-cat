<?php

// Import the CI class
require_once 'application/libraries/CI.php';

// Create a new CI instance
$CI = new CI();

// Define the model class
class ChatbotModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        // Set the database table name
        $this->table = 'chatbot';
    }

    public function getChatbot($chatbotID = null)
    {
        // If no chatbot ID is specified, get all chatbots
        if ($chatbotID === null) {
            $query = $this->db->get($this->table);

            // Return the chatbots
            return $query->result();
        }

        // Get the chatbot with the specified ID
        $query = $this->db->get_where($this->table, ['chatbotID' => $chatbotID]);

        // Return the chatbot
        return $query->row();
    }

    public function createChatbot($data)
    {
        // Insert the chatbot data into the database
        $this->db->insert($this->table, $data);

        // Return the chatbot ID
        return $this->db->insert_id();
    }

    public function updateChatbot($chatbotID, $data)
    {
        // Update the chatbot data in the database
        $this->db->update($this->table, $data, ['chatbotID' => $chatbotID]);

        // Return the success message
        return ['message' => 'Chatbot message updated successfully'];
    }

    public function deleteChatbot($chatbotID)
    {
        // Delete the chatbot from the database
        $this->db->delete($this->table, ['chatbotID' => $chatbotID]);

        // Return the success message
        return ['message' => 'Chatbot message deleted successfully'];
    }
}

// Get all chatbots
$chatbots = $chatbotModel->getChatbot();

// Get a chatbot with the specified ID
$chatbot = $chatbotModel->getChatbot(1);

// Create a new chatbot
$data = [
    'time' => '12:00:00',
    'pesan' => 'Hello, how can I help you?',
    'dokterID' => 1,
    'userID' => 1,
    'fotoUs' => 'https://example.com/foto.jpg',
];
$chatbotID = $chatbotModel->createChatbot($data);

// Update a chatbot
$data = [
    'pesan' => 'How are you today?',
];
$chatbotModel->updateChatbot($chatbotID, $data);

// Delete a chatbot
$chatbotModel->deleteChatbot(1);
