<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_model extends CI_Model {

	public function select_all() {
		$this->db->select('*');
		$this->db->from('laporan AS lp ');
		$this->db->join('user AS us ', 'lp.id_user = us.id_user');		
		$this->db->join('dokter AS dk ', 'lp.id_dokter = dk.id_dokter');	
		$data = $this->db->get();
		return $data->result();
	}

	public function select_by_id($id) {
		$this->db->where('LapID',$id);
		$data = $this->db->get("laporan");		
		return $data->row();
	}

	public function insert($data) {
		$data = array(
			'Rincian' => $data['Rincian'],
			'Total' => $data['Total'],
			'id_user' => $data['id_user'],
			'id_dokter' => $data['id_dokter'],
            'Sesi' => $data['Sesi']
            
		);
		$this->db->insert('laporan', $data);
		return $this->db->affected_rows();
	}

	public function insert_batch($data) {
		$this->db->insert_batch('laporan', $data);
		return $this->db->affected_rows();
	}

	public function update($data) {
		$list = array(
			'Rincian' => $data['Rincian'],
			'Total' => $data['Total'],
			'id_user' => $data['id_user'],
			'id_dokter' => $data['id_dokter'],
            'Sesi' => $data['Sesi']
		);
		$this->db->where('LapID',$data['LapID']);
		$this->db->update('laporan', $list);				
		return $this->db->affected_rows();
	}

	public function delete($id) {
		$this->db->where('LapID', $id);
		$this->db->delete('laporan');
		return $this->db->affected_rows();
	}

	public function check_nama($nama) {
		$this->db->where('LapID', $nama);
		$data = $this->db->get('laporan');
		return $data->num_rows();
	}

	public function total_rows() {
		$data = $this->db->get('laporan');
		return $data->num_rows();
	}
}

/* End of file M_kota.php */
/* Location: ./application/models/M_kota.php */