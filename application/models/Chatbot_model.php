<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chatbot_model extends CI_Model {


	public function select_all() {
		$this->db->select('*');
		$this->db->from('chatbot AS cb ');
		$this->db->join('user AS us ', 'cb.id_user = us.id_user');		
		$this->db->join('dokter AS dk ', 'cb.id_dokter = dk.id_dokter');	
		$data = $this->db->get();
		return $data->result();
	}

	// public function select_all() {		
	// 	$data = $this->db->get("chatbot");
	// 	return $data->result();
	// }

	public function select_by_id($id) {
		$this->db->where('ChatbotID',$id);
		$data = $this->db->get("chatbot");		
		return $data->row();
	}

	public function insert($data) {
		$data = array(
			'Id_user' => $data['id_user'],
			'Id_dokter' => $data['id_dokter'],
			'time' => $data['Time'],
			'pesan' => $data['Pesan']
		);
		$this->db->insert('chatbot', $data);
		return $this->db->affected_rows();
	}

	public function insert_batch($data) {
		$this->db->insert_batch('chatbot', $data);
		return $this->db->affected_rows();
	}

	public function update($data) {
		$list = array(
			'Id_user' => $data['id_user'],
			'Id_dokter' => $data['id_dokter'],
			'time' => $data['Time'],
			'pesan' => $data['Pesan']
		);
		$this->db->where('ChatbotID',$data['chatbot_id']);
		$this->db->update('chatbot', $list);				
		return $this->db->affected_rows();
	}

	public function delete($id) {
		$this->db->where('ChatbotID', $id);
		$this->db->delete('chatbot');
		return $this->db->affected_rows();
	}

	public function check_nama($nama) {
		$this->db->where('Time', $nama);
		$data = $this->db->get('chatbot');
		return $data->num_rows();
	}

	public function total_rows() {
		$data = $this->db->get('chatbot');
		return $data->num_rows();
	}
}

/* End of file M_kota.php */
/* Location: ./application/models/M_kota.php */