<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('m_artikel');

    }
    public function index(){
        $data['title'] = "Artikel";
        $this->load->view('header_page',$data);
        $this->load->view('v_article',$data);
        $this->load->view('footer_page');
    }
    public function artikel($id){
        $this->form_validation->set_rules('kategori_artikel','Kategori_artikel','required|trim');
        $this->form_validation->set_rules('judul_artikel','Judul_Artikel','required|trim');
        $this->form_validation->set_rules('konten_artikel','Konten_artikel','required|trim');
        $this->form_validation->set_rules('sumber_artikel','Sumber_Artikel','required|trim');
        if ($this->form_validation->run() === false){
            redirect('artikel');
        }else{
            $userdata = $this->session->all_userdata();
            $kategori_artikel = $this->input->post('kategori_artikel');
            $judul_artikel = $this->input->post('judul_artikel');
            $konten_artikel = $this->input->post('konten_artikel');
            $sumber_artikel = $this->input->post('sumber_artikel');
            $appt = $this->input->post('appt');
            $data = [
                "id_artikel" => $id_artikel,
                "kategori_artikel"=> $kategori_artikel,
                "konten_artikel"=>$konten_artikel,
                "sumber_artikel"=>$sumber_artikel

            ];
            $cekdb = $this->db->insert('artikel',$data);
            if($cekdb){
                $artikel = $this->db->get_where('artikel',array("id_artikel"=>$id))->row_array();
                redirect('artikel/');
            }else{
                redirect('cariartikel');
            }

            
            
        }
        

    }
}