// Buat sebuah controller di CodeIgniter
class Pembayaran extends CI_Controller {
    
    public function index() {
        // Mengambil data dari tabel user
        $data['user'] = $this->db->get('user')->result();
        
        // Mengambil data dari tabel dokter
        $data['dokter'] = $this->db->get('dokter')->result();
        
        // Mengambil data dari tabel chatbot
        $data['chatbot'] = $this->db->get('chatbot')->result();
        
        // Misalnya, Anda ingin mengambil transaksi yang terkait dengan user tertentu, dokter tertentu, dan chatbot tertentu
        $user_id = 1; // Ganti dengan ID user yang sesuai
        $dokter_id = 2; // Ganti dengan ID dokter yang sesuai
        $chatbot_id = 3; // Ganti dengan ID chatbot yang sesuai

        // Query untuk mengambil data transaksi pembayaran
        $this->db->select('waktu, tanggal, nama_user, nama_dokter, harga');
        $this->db->from('pembayaran'); // Ganti 'pembayaran' dengan nama tabel transaksi yang sesuai
        $this->db->join('user', 'pembayaran.id_user = user.id_user');
        $this->db->join('dokter', 'pembayaran.id_dokter = dokter.id_dokter');
        $this->db->join('chatbot', 'pembayaran.ChatbotID = chatbot.ChatbotID');
        $this->db->where('pembayaran.id_user', $user_id);
        $this->db->where('pembayaran.id_dokter', $dokter_id);
        $this->db->where('pembayaran.ChatbotID', $chatbot_id);
        $data['transaksi_data'] = $this->db->get()->result();

        // Tampilkan view dengan data
        $this->load->view('bukti_pembayaran', $data);
    }
}
