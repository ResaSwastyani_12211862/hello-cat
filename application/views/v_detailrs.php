<?php
defined ('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?= $title ?></title>
<style media="screen">
.inputsetting {
  width: 60%;
  padding: 6px 20px;
  box-sizing: border-box;
  border: none;
  border-bottom: 2px solid grey;
  margin-left: 20px;
  margin-top: 1px;
  outline: none;
}
.inputsetting:active, .inputsetting:hover{
  border-color: #ec4638;
}
.label{
  padding-top: 5px;
  margin-top: 10px;
  /* margin-left: 20px; */
}
.boxsetting{
  width: 300px;
  height: 200px;
  padding: 0 30px 30px 30px ;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  width: 500px;
  align-items: center;
  margin-top: 5px;
}


  .container {
    max-width: 100%; 

  }

  .table {
    width: 60%;
    border-collapse: collapse;
    margin-top: 2%;
  }

  .table img {
    max-width: 100%; 
    height: auto; 
    display: block; 
    margin: 0 auto; 
    max-height: 100px;
  

  }

  img {
  max-width: 100%;
  height: auto;
  display: block;
  margin: auto;
  margin-top: 150px;
  }

  h3 b {
      font-size: 45px; 
  }

  .image-wa1 {
    margin-top: -200px; 
    margin-left: 600px;
    max-width: 600px;
    max-height: 600px;
  }

  .image-wa2 {
    margin-top: -650px; 
    margin-right: 700px;
    max-width: 400px;
    max-height: 400px;
  }

  body {
    background-image: url("<?php echo base_url(); ?>/assets/bgh.png"); 
    background-size: cover; 
    background-repeat: no-repeat; 
    background-position: center; 
    background-color: #FFDAB9;
  }

  .outer-box {
    background-color: #FFDEAD;
    padding: 10px; 
    box-sizing: border-box;
    margin-left: -85px; 
    margin-right: -85px; 
  }

  .outer-box img {
    width: 100%;
    height: auto;
    display: block;
  }

</style>

</style>
</head>

<body>
	<main>
<div class="container">
  <table class="table table-striped"   style="margin-top: 10%;">
  <div class="outer-box">
      <img src="<?php echo base_url(); ?>/assets/hello.png" alt="Hello Image" width="500">
  </div>
  <br> <br>
  <h3>
    <b>TIPS MERAWAT KUCING KESAYANGAN</b> <br>
    <b>ANDA</b>
  </h3>
  <br>
  <h2>
      Merawat kucing kesayangan dengan baik memerlukan perhatian, cinta, dan perawatan yang baik. Berikut beberapa tips untuk merawat kucing Anda dengan baik: <br> <br>
  <b> 1. Nutrisi yang Baik </b> <br>
      Berikan makanan berkualitas tinggi yang sesuai dengan usia, berat badan, dan kebutuhan kesehatan kucing Anda. Diskusikan dengan dokter hewan mengenai diet yang tepat untuk kucing Anda. <br> <br>
  <b> 2. Air Bersih </b> <br>
      Pastikan selalu ada akses ke air bersih, segar, dan cuci mangkuk air secara teratur.<br> <br>
  <b> 3. Bersihkan Kotak Pasir </b> <br>
      Bersihkan kotak pasir setiap hari, dan ganti pasir secara berkala. Gunakan pasir yang disukai oleh kucing Anda. <br> <br>
  <b> 4. Vaksinasi dan Perawatan Kesehatan Rutin </b> <br>
      Jadwalkan kunjungan rutin ke dokter hewan untuk vaksinasi, pemeriksaan kesehatan, dan perawatan lainnya sesuai dengan jadwal yang disarankan oleh dokter hewan. <br> <br>
  <b> 5. Perawatan Gigi </b> <br>
      Sisir bulu kucing Anda secara teratur untuk menghindari keguguran bulu dan membantu mengurangi bercak bulu. Berikan makanan khusus untuk membersihkan gigi atau pertimbangkan untuk menyikat gigi kucing Anda jika memungkinkan. <br> <br>
  <b> 6. Mainan dan Aktivitas </b> <br>
      Berikan mainan dan aktivitas yang cocok untuk kucing Anda. Kucing senang bermain dan berolahraga. Mainan interaktif dan pohon kucing bisa membantu menjaga kucing tetap aktif. <br> <br>
  <b> 7. Beri Kasih Sayang </b> <br> 
      Kucing adalah hewan yang memerlukan perhatian dan kasih sayang. Luangkan waktu untuk bermain, membelai, dan bercakap dengan kucing Anda. Ini penting untuk membangun ikatan yang kuat. <br> <br>   
  <b> 8. Lingkungan yang Aman </b> <br>  
      Pastikan rumah Anda aman bagi kucing. Hindari bahan berbahaya, seperti tanaman beracun dan makanan beracun. Selain itu, pastikan jendela dan balkon terjaga agar kucing tidak terjatuh. <br> <br>
  <b> 9. Sterilisasi/Kastrasi </b> <br>  
      Berbicaralah dengan dokter hewan Anda mengenai sterilisasi/kastasi kucing Anda. Selain mengendalikan populasi kucing, ini juga dapat membantu mengurangi risiko beberapa masalah kesehatan. <br> <br>
      Ingatlah bahwa setiap kucing memiliki kebutuhan yang berbeda, jadi perhatikan kucing Anda secara individual dan tanggapi kebutuhan dan keinginannya dengan penuh kasih sayang. Kucing yang mendapatkan perawatan yang baik, kasih sayang, dan perhatian akan lebih bahagia dan sehat.


      <br>
      <br>
      <br>
      <br>
  </h2>
  <div class="image-wa1">
      <img src="<?php echo base_url(); ?>/assets/wa1.png" alt="..." >
  </div>
  <div class="image-wa2">
      <img src="<?php echo base_url(); ?>/assets/wa2.png" alt="..." >
  </div>
  </main>
</body>
</html>