<?php
defined ('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?= $title ?></title>
<style media="screen">
.inputsetting {
  width: 60%;
  padding: 6px 20px;
  box-sizing: border-box;
  border: none;
  border-bottom: 2px solid grey;
  margin-left: 20px;
  margin-top: 1px;
  outline: none;
}
.inputsetting:active, .inputsetting:hover{
  border-color: #ec4638;
}
.label{
  padding-top: 5px;
  margin-top: 10px;
  /* margin-left: 20px; */
}
.boxsetting{
  width: 300px;
  height: 200px;
  padding: 0 30px 30px 30px ;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  width: 500px;
  align-items: center;
  margin-top: 5px;
}


  .container {
    max-width: 100%; 

  }

  .table {
    width: 60%;
    border-collapse: collapse;
    margin-top: 2%;
  }

  .table img {
    max-width: 100%; 
    height: auto; 
    display: block; 
    margin: 0 auto; 
    max-height: 100px;
  

  }

  img {
  max-width: 100%;
  height: auto;
  display: block;
  margin: auto;
  margin-top: 150px;
  }

  h3 b {
      font-size: 45px; 
  }

  .image-h21 {
    margin-top: -150px; 
    margin-left: 600px;
    max-width: 600px;
    max-height: 600px;
  }

  .image-h22 {
    margin-top: -750px; 
    margin-right: 700px;
    max-width: 400px;
    max-height: 400px;
  }

  body {
    background-image: url("<?php echo base_url(); ?>/Assets/bgh.png"); 
    background-size: cover; 
    background-repeat: no-repeat; 
    background-position: center; 
    background-color: #FFDAB9;
  }

  .outer-box {
    background-color: #FFDEAD;
    padding: 10px; 
    box-sizing: border-box;
    margin-left: -85px; 
    margin-right: -85px; 
  }

  .outer-box img {
    width: 100%;
    height: auto;
    display: block;
  }

</style>

</style>
</head>

<body>
	<main>
<div class="container">
  <table class="table table-striped"   style="margin-top: 10%;">
  <div class="outer-box">
      <img src="<?php echo base_url(); ?>/Assets/hello.png" alt="Hello Image" width="500">
  </div>
  <br> <br>
  <h3>
    <b>WASPADA!</b> <br>
    <b>PENYAKIT KUCING YANG SERING</b> <br>
    <b>DIANGGAP SEPELE</b>
  </h3>
  <br>
  <h2>
  <b> 1. Flu Kucing (Feline Upper Respiratory Infection) </b> <br>
      Infeksi saluran pernapasan atas pada kucing sering disebabkan oleh virus dan dapat menyebabkan gejala seperti bersin, hidung berair, dan mata berair. Meskipun seringkali penyakit ini sembuh dengan sendirinya, dalam beberapa kasus, itu bisa berkembang menjadi sesuatu yang lebih serius seperti pneumonia. <br> <br>
  <b> 2. Infeksi Saluran Kemih </b> <br>
      Infeksi saluran kemih (UTI) pada kucing bisa menyebabkan gejala seperti sering buang air kecil di luar kotak pasir, mengeong saat buang air kecil, dan darah dalam urin. UTI bisa menjadi masalah serius jika tidak diobati, terutama pada kucing jantan, karena mereka dapat mengalami penyumbatan uretra. <br> <br>
  <b> 3. Gingivitis dan Penyakit Gusi </b> <br>
      Masalah pada mulut kucing, seperti radang gusi, sering diabaikan. Gingivitis dapat berkembang menjadi penyakit gusi yang lebih serius, seperti periodontitis, yang dapat menyebabkan kerusakan gigi dan bahkan masalah kesehatan secara keseluruhan. <br> <br>
  <b> 4. Cacingan </b> <br>
      Infeksi cacing pada kucing adalah masalah umum yang sering dianggap sepele. Cacing usus dapat menyebabkan masalah pencernaan, penurunan berat badan, dan bahkan bisa menjadi ancaman serius bagi kesehatan kucing jika tidak diobati. <br> <br>
  <b> 5. Kerontokan Bulu (Falling Fur) </b> <br>
      Banyak pemilik kucing mungkin mengira kerontokan bulu adalah hal biasa, tetapi jika itu berlebihan atau disertai dengan gejala lain seperti kulit gatal atau ruam, itu bisa menjadi tanda alergi, infeksi, atau masalah kulit lainnya. <br> <br>
  <b> 6. Obesitas </b> <br>
      Kucing yang terlalu gemuk sering dianggap lucu atau menggemaskan, tetapi obesitas adalah masalah serius yang dapat menyebabkan berbagai masalah kesehatan, termasuk diabetes, penyakit jantung, dan masalah persendian. <br> <br>
  </h2>
  <div class="image-h21">
      <img src="<?php echo base_url(); ?>/Assets/h21.png" alt="..." >
  </div>
  <div class="image-h22">
      <img src="<?php echo base_url(); ?>/Assets/h22.png" alt="..." >
  </div>
  </main>
</body>
</html>
