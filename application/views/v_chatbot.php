<?php include('server.php') ?>
<html>
<head>
<style>
  body {
    font-family: 'Montserrat', sans-serif;
    background-image:  url("<?php echo base_url(); ?>/Assets/bgh.png");
    color: #495057;
  }

  .cont {
    margin-top: 20px;
  }

  h1 {
    color: #007bff;
  }

  #chat {
    margin-top: 20px;
    padding: 10px;
    border: 1px solid #ddd;
    height: 150px;
    overflow-y: scroll;
  }

  #userInput {
    margin-top: 10px;
    width: 70%;
    padding: 10px;
    font-size: 16px;
  }

  button {
    margin-top: 10px;
    padding: 10px;
    font-size: 16px;
    background-color: #007bff;
    color: #fff;
    border: none;
    cursor: pointer;
  }

  #form1 {
    margin-top: 20px;
  }

  .form-group {
    margin-bottom: 20px;
  }

  input {
    width: 100%;
    padding: 10px;
    font-size: 16px;
    position: fixed;
  }

  .header {
    background-color: #FFDEAD;
    padding: 5px;
    text-align: center;
    position: fixed;
    width: 100%;
    height: 19%;
    top: 0;
    z-index: 1000;
  }

  iframe {
    width: 100%;
    height: calc(100vh - 30px);
  }

  .header img {
    width: 80px;
    height: 90px; 
    border-radius: 60%; 
    margin-right: 1100px;
    margin-top: 10px;
  }

  .header .name {
    color: black;
    font-size: 18px;
    font-weight: bold;
    display: inline-block;
    margin-top: -60px;
    margin-right: 700px;
  }

  .header .icon {
    margin-right: 30px; 
    font-size: 24px; 
  }

  #timer {
  font-size: 24px;
  position: absolute;
  right: 10px;
  top: 50%;
  transform: translateY(-50%);
  color: #808080;
}

#stopwatch-icon {
  font-size: 24px;
  position: absolute;
  right: 75px;
  top: 50%;
  transform: translateY(-50%);
  color: #808080;
}

/* CSS for overlay */
.overlay {
  display: none;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5); 
  justify-content: center;
  align-items: center;
}

/* CSS for overlay content */
.overlay-content {
  background: #fff; 
  padding: 20px;
  text-align: center;
}

/* CSS for the message */
.message {
  font-size: 24px;
  color: #f00; 
}

</style>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700,800,900|Ubuntu" rel="stylesheet">
  <link href="css/chat.css" rel="stylesheet" >
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha384-GLhlTQ8iK9tGR8r29ZLnnrHjpWU9L/feF +46P7LbHaXnj1lHDt4NfPOemjIb6FfF" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

</head>

<body>

  <div class="cont">
    <?php if(isset($_SESSION['nama_user'])): ?>
      <button class="btn btn-light btn-lg" type="button" name="button"><a href="index.php?logout='1'" style="color:black;text-decoration: none;padding:1rem;">Logout</a></button>
      <button id="formButton" class="btn btn-light btn-lg" type="button" name="button"><a href="chat.php#form1" style="color:black;text-decoration: none;">Update Info</a></button>
      <h1>Welcome <?php echo (string)$_SESSION['nama_user'];?></h1>
    <?php endif ?>
  </div>

  <div class="header">
  <span class="icon">
    <i class="fas fa-arrow-left"></i>
  </span>
  <img src="<?php echo base_url(); ?>/Assets/pita.png" alt="...">
  <span class="name">Dr. Fitri Novitasari, DVM</span>
  <span id="timer">01:00</span>
  <span id="stopwatch-icon" class="icon">
    <i class="fas fa-stopwatch"></i>
  </span>
</div>

<div class="overlay" id="overlay">
    <div class="overlay-content">
      <div class="message">Mohon maaf.. Waktu berkonsultasi sudah habis!!</div>
    </div>
  </div>

  <iframe allow="microphone;" src="https://console.dialogflow.com/api-client/demo/embedded/0f9d225e-5c3d-4260-8b99-17dd646e5f1d"></iframe>
    
<script>
  // Set waktu awal dalam detik (1 menit)
var countdown = 1 * 60;

// Fungsi untuk mengupdate timer
function updateTimer() {
  var hours = Math.floor(countdown / 3600);
  var minutes = Math.floor((countdown % 3600) / 60);
  var seconds = countdown % 60;

  // Tambahkan 0 di depan jika jam, menit, atau detik kurang dari 10
  var formattedTime = hours.toString().padStart(2, '0') + ':' +
                      minutes.toString().padStart(2, '0') + ':' +
                      seconds.toString().padStart(2, '0');

  // Perbarui teks timer
  document.getElementById('timer').textContent = formattedTime;

  // Kurangi satu detik
  countdown--;

  // Hentikan timer jika sudah mencapai 0
if (countdown < 0) {
  clearInterval(timerInterval);

  // Tampilkan overlay
  document.getElementById('overlay').style.display = 'flex';
}
}

// Panggil fungsi updateTimer setiap detik
var timerInterval = setInterval(updateTimer, 1000);
</script>
  </body>
</div>
</html>
