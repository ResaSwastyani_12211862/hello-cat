<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?= $title ?></title>
	<style>
		*{
			margin: 0;
			padding: 0;
			box-sizing: border-box;
			text-decoration: none;
		}
		body{
			font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';
			overflow: hidden;
			background-image:  url("<?php echo base_url(); ?>/Assets/bgh.png");
			
			background-repeat: no-repeat;
/*			background-color: white;*/
			background-size: cover;
		}
		/* .background{
			background-image:  ;
			background-repeat: no-repeat;
			background-color: white;
			background-size: cover;
			height: 100vh;
			display: flex;
		} */

		
		.box {
			background-color: #FFDAB9;
			padding: 20px;
			border-radius: 10px;
			box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
			margin-top: 25vh;
			margin-left: 65%;
			margin-right: 120px; 
			font-weight: 300px;
		}


/*		.text{
			margin-left: 10%;
			font-weight: 300px;
		}*/
/*		.box{
			margin-left: 40%;
		}*/
		.texth1{
			font-size: 45px;
			color: #000;
			font-weight: 500;
		}
		.textp{
			font-size: 16px;
			color: #000;
			font-weight: 300;
		}
		.texta{
			color: #000;
			font-weight: 700;
		}
		.texta:hover{
			color: #ec4638;
		}
		.textf{
			color: #000;
			font-weight: 700;
			font-size: 12px;
		}
		.textf:hover{
			color: #ec4638;
		}
		.formlogin{
			background: transparent;
			color: #000;
			box-sizing: border-box;
			display: flex;
			flex-direction: column;
			width: 250px;
		}
		input{
			margin: 20px 0;
			padding: 10px;
			background: transparent;
			border: none;
			outline: none;
			color: #7f91a1;
			font-weight: 500;
			font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';
		}
		button{
			margin: 20px 0;
			padding: 10px;
			background-color: transparent;
			border: none;
			border: 2px solid #ec4638;
			color: #ec4638;
			border-radius: 20px;
			font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';
			font-size: 16px;
		}
		button:hover{
			background: #ec4638;
			color: #fff;
			cursor: pointer;
		}
		.email, .password{
			border-bottom: 1px solid #ec4638;
		}
		.login-image {
			position: absolute;
			top: 20px;
			left: 85px;
			width: 50%;
			height: 100%;
			background-image: none; /* Remove background image property */
			float: left; /* Make the image float to the left */
		}

		.login-image img {
			width: 100%;
			height: auto;
			display: block;
		}

		.forgot-password {
 			 font-size: 12px;
		}

		.or-continue-with {
  			text-align: center;
		}
		.email {
			border: 1px solid #ccc;
			padding: 5px;
			border-radius: 4px;
			width: 100%;
		}
		.password {
			border: 1px solid #ccc;
			padding: 5px;
			border-radius: 4px;
			width: 100%;
		}
		img.sosial {
			width: 25px;
			height: 25px;
		}
		.social-icons {
  			text-align: center;
			display: flex;
			justify-content: space-around;
			padding: 10px;
		}
		.social-icons a:hover img {
  filter: brightness(1.2) contrast(1.2); /* Adjust brightness and contrast on hover */
  transform: scale(1.1); /* Slightly enlarge the icon on hover */
}


		
		.login-hello img {
			position: relative;
			bottom: 600px;
			width: 20%;
			margin-left: 925px;
			margin-right: auto;
		}

	</style>
</head>

<body>
	<main>

		<div class="background">
			<div class="box">
				<h1 class="texth1">Login</h1>
				<br>
				<?php if ($this->session->flashdata('flash')) : ?>
					<div class="row mt-3">
						<div class="col-md-6">
							<div class="alert alert-danger alert-dismissible fade show" role="alert">
								<?= $this->session->flashdata('flash'); ?>
								<!-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button> -->
							</div>
						</div>
					</div>
					<?php endif; ?>
				<form class="formlogin" method = 'post' action='<?= base_url('user/signin')?>'>
				<label for="email">Email</label>
					<input type="text" class="email" name="email" placeholder="Email" value="<?= set_value('email')?>">
					<?= form_error("email",'<small class= "text-danger pl-3">','</small>')?>
					<label for="email">Password</label>
					<input type="password" class="password" name="password" placeholder="Password" >
					<?= form_error("password",'<small class= "text-danger pl-3">','</small>')?>
					<a href="<?= base_url('user/signup')?>" class="textf">Forgot Password</a>
					<button type="submit" value="Sign In" class="button" >Sign In</button>
					<div class="or-continue-with">or continue with</div>
					<div class="social-icons">
					<a href="https://mail.google.com/">
					<img src="<?php echo base_url(); ?>/Assets/google.png" class="sosial" alt="..." > 
					</a>
					<a href="https://github.com/">
					<img src="<?php echo base_url(); ?>/Assets/github.png" class="sosial" alt="..." >
					</a>
					<a href="https://www.facebook.com/">
					<img src="<?php echo base_url(); ?>/Assets/facebook.png" class="sosial" alt="..." >
					</a>
					</div>


				</form>
				<p class="textp">Don't have an account yet? <a href="<?= base_url('user/signup')?>" class="texta">Register for free</a></p>
			</div>
		</div>
		<div class="login-image">
        <img src="<?php echo base_url(); ?>/Assets/login.png" alt="..." >
   	 </div>
		<div class="login-hello">
        <img src="<?php echo base_url(); ?>/Assets/hello.png" alt="..." >
   	 </div>
	</main>
</body>
</html>
